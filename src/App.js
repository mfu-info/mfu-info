import React from 'react';
import './App.css';
import 'antd/dist/antd.css';
import Index from './Components';


function App() {
  return (
    <div className="App">
      <Index/>
    </div>
  );
}

export default App;

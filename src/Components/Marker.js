import React, { Component, Fragment } from 'react';
import './css/Marker.css';
import { Modal, Button, Statistic, Descriptions, Steps, Divider, Popover, Typography, Tag, Avatar, Carousel } from 'antd';
import { objectToArray } from './ObjectToArray'


class Marker extends Component {

  state = {
    loading: false,
    visible: false,
  };

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = () => {
    this.setState({ loading: true });
    setTimeout(() => {
      this.setState({ loading: false, visible: false });
    }, 3000);
  };

  handleCancel = () => {
    this.setState({ visible: false });
  };

  onFinish() {
    console.log('finished!');
  }

  render() {


    const { Step } = Steps;
    const { visible } = this.state; // loading
    const { name, detail } = this.props; // color, id
    // Countdown
    const { Countdown } = Statistic;
    const deadline = Date.now() + 1000 * 60 * 15 + 1000;

    // Text
    const { Text } = Typography;

    return (
      <Fragment>
        <Popover content={detail.title} trigger="hover">
          <Avatar
            src={detail.image}
            className="marker"
            icon="user"
            onClick={this.showModal}
            style={{ cursor: 'pointer' }}
            title={name}
          >

          </Avatar>
          <div className="pulse" />

          <Modal
            centered
            visible={visible}
            title={detail.title}
            onOk={this.handleOk}
            onCancel={this.handleCancel}
            // footer={[
            //   <Button key="back" onClick={this.handleCancel}>
            //     Return
            // </Button>,
            //   <Button key="submit" type="primary" loading={loading} onClick={this.handleOk}>
            //     Submit
            // </Button>,
            // ]}
            footer={[
              <Button key="back" type="primary" onClick={this.handleCancel}>
                ปิด
            </Button>
            ]}
          >
            <p>{detail.typePlace}</p>

            {detail.type !== "people" && (
              <Fragment>
                <br />
                <Carousel autoplay>
                  {detail.gallery && objectToArray(detail.gallery).map((images, index) => (
                    <div>
                      <img
                        src={images.image}
                        alt={index}
                        style={{ width: "inherit", height: "300px!important"}} />
                    </div>
                  ))}
                </Carousel>
                <br />
              </Fragment>
            )}

            <p>{detail.description}</p>

            {/* Busstop Only */}
            {/* ======================================================================== */}
            {detail.type === "busstop" && (
              <Fragment>
                <Descriptions size="small" column={2}>
                  <Descriptions.Item label="ประเภทรถ">{detail.carType}</Descriptions.Item>
                  <Descriptions.Item label="จำนวนที่นั่ง">12 ที่นั่ง</Descriptions.Item>
                  <Descriptions.Item label="ระยะเวลารอ"> 15 นาที/รอบ</Descriptions.Item>
                  <Descriptions.Item label="อัพเดตล่าสุด">2019-10-10</Descriptions.Item>
                </Descriptions>
                <br />

                <Divider />
                <br />
                <Steps progressDot current={0}>
                  <Step title={detail.start} description="จุดขึ้นรถ" />
                  <Step title={detail.end} description="จุดถัดไป" />
                </Steps>
                <br />
                <Divider />

                <br />
                <Countdown title="รอบถัดไปอีก" value={deadline} onFinish={this.onFinish} />

                <br />
                <Descriptions size="small">
                  <Descriptions.Item label="หมายเหตุ">
                    <Text code type="danger">
                      รอบและระยะเวลาอาจะมีการคลาดเคลื่อนได้
                  </Text>
                  </Descriptions.Item>
                </Descriptions>
              </Fragment>
            )}

            {/* Building Only */}
            {/* ======================================================================== */}
            {detail.type === "building" && (
              <Fragment>
                <br />
                <h4 style={{ margin: '16px 0' }}>ห้อง:</h4>
                <div>
                  {detail.rooms && objectToArray(detail.rooms).map((rooms, index) => (
                    <Tag color="#2db7f5" key={index}>{rooms.room}</Tag>
                  ))}
                </div>
              </Fragment>
            )}

            {detail.type !== "people" && (
              <Fragment>
                <br />
                <Statistic
                  title="เวลาให้บริการ"
                  prefix=""
                  value={detail.time}
                  style={{
                    fontSize: '8px',
                    // margin: '0 15px'
                  }}
                />
              </Fragment>
            )}

          </Modal>
        </Popover>
      </Fragment>
    );
  };
}

export default Marker;
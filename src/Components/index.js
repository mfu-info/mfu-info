import React, { Component } from 'react'
import GoogleMapReact from 'google-map-react';
import Marker from './Marker';
import json from './place.json';
import { PageHeader, Tag, Button } from 'antd';
import { Link,Router } from 'react-router-dom'


export default class Index extends Component {
    state = {
        center: {
            lat: 20.045036,
            lng: 99.894184
        },
        zoom: 18,
        GeocoderLocationType: {
            lat: '',
            lng: ''
        },
        places: []
    };

    shouldComponentUpdate(nextProps, nextState) {
        console.log(nextState.center)
        if (nextState.center) {
            return true
        }
    }

    handleLocation = (e) => {
        e.preventDefault();
        navigator.geolocation.getCurrentPosition(position => {
            const pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            }
            this.setState({ center: pos })
            
        })

    }

    getMapOptions = (maps) => {
        return {
            disableDefaultUI: false,
            mapTypeControl: true,
            streetViewControl: true,
            fullscreenControl: false,
            styles: [{ featureType: 'poi', elementType: 'labels', stylers: [{ visibility: 'on' }] }],
        };
    };

    render() {
        return (
            <div style={{ height: '100vh', width: '100%' }}>
                
                <PageHeader
                    title="MFU. info"
                    style={{
                        // position: "absolute",
                        zIndex: 999,
                        border: '1px solid rgb(235, 237, 240)',
                    }}
                    subTitle="Mae Fah Luang Location"
                    // tags={<Tag color="blue">Beta</Tag>}
                    avatar={{ src: './mfu-logo.png' }}
                    extra={[
                        <Button key="1" type="primary" onClick={this.handleLocation}>
                            My Location
                        </Button>,
                    ]}
                />
                <GoogleMapReact
                    bootstrapURLKeys={{ key: "AIzaSyDpvg4mFKNn2hHBC4ERF6nhvpyScxNV4W8" }}
                    defaultCenter={this.state.center}
                    defaultZoom={this.state.zoom}
                    options={this.getMapOptions}
                >

                    {json.map((mapDetail, index) => {
                        console.log("map >> ", mapDetail.title)
                        return <Marker
                            key={index}
                            detail={mapDetail}
                            lat={mapDetail.lat}
                            lng={mapDetail.long}
                            text={mapDetail.title}
                            color="blue"
                        />
                    })}

                </GoogleMapReact>
                
            </div>
        )
    }
}
